const jwt = require("jsonwebtoken");
const secret = "CourseBookingAPI";


// JSON web token is a way of securrely passing information


module.exports.createAccessToken = (user) =>{
	const data = {
		id : user._id,
		email : user.email,
		isAdmin : user.isAdmin
	};
	return jwt.sign(data,secret,{});
}