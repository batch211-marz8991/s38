const User = require("../models/User");
const bcrypt =require("bcrypt")
const auth = require("../auth")

//check if the email already exist
/*
	Steps
		1. use mongoose method to find duplicate emails
		2. use the "then" method to send a response back to the FE application


*/

module.exports.checkEmailExists = (reqBody)=>{
	return User.find({email:reqBody.email}).then(
		result=>{
			if(result.length>0){
				return true;
			}else{
				return false;
			};
		});
};

module.exports.registerUser = (reqBody)=>{
	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		mobileNo : reqBody.mobileNo,
		password : bcrypt.hashSync(reqBody.password,10)
	})

	return newUser.save().then((user,error)=>{
		if(error){
			return false;
		}else{
			return true;
		}
	})
}

module.exports.loginUser = (reqBody) =>{
    return User.findOne({email:reqBody.email}).then(result=>{
        if(result==null){
            return false;
        }else{
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password,result.password);
            if(isPasswordCorrect){
                return {access: auth.createAccessToken(result)}
            }else{
                return false
            };
        };
    })
};


module.exports.getProfile = (data) =>{
    return User.findById(data.id).then(result=>{
        result.password = "";
        return result;
    });
}
